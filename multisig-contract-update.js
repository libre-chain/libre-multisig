const fs = require('fs');
const { Api, JsonRpc } = require('eosjs');
const { Serialize } = require('eosjs');
const rpc = new JsonRpc('https://testnet.libre.org'); // replace with your preferred EOS node

// vars to change
const proposer = `quantum`;
const proposalName = 'systemupdate' // makeid(10) - for random name
const contractAccountName = 'eosio'; // account executing transaction
const wasmPath = './contracts/libre.system/libre.system.wasm'; // replace with the path to your wasm file
const abiPath = './contracts/libre.system/libre.system.abi'; // replace with the path to your abi file
const requestedActor = 'board.libre';
const requestedActorPermission = 'active';

const getRequestedSigners = async(actor, permissionName) => {
    const account = await rpc.get_account(actor);
    const permission = account.permissions.find(c=> permissionName === c.perm_name);
    const requiredSigners = permission.required_auth.accounts.map((account) => {
        return {...account.permission};
    });
    console.log(requiredSigners)
    return requiredSigners;
}

const expirationDateObj = new Date();
expirationDateObj.setDate(expirationDateObj.getDate() + 7);
const expirationDate = expirationDateObj.toISOString().substring(0,19); 

const api = new Api({
    rpc: rpc,
    textDecoder: new TextDecoder(),
    textEncoder: new TextEncoder(),
  });

const run = async() => {
    const signers = await getRequestedSigners(requestedActor, requestedActorPermission);
    // read the wasm and abi files
    const wasm = fs.readFileSync(wasmPath);
    const abi = fs.readFileSync(abiPath);

    const buffer = new Serialize.SerialBuffer({
        textEncoder: api.textEncoder,
        textDecoder: api.textDecoder,
    })
    
    let abiJSON = JSON.parse(abi)
    const abiDefinitions = api.abiTypes.get('abi_def')
    abiJSON = abiDefinitions.fields.reduce(
        (acc, { name: fieldName }) =>
            Object.assign(acc, { [fieldName]: acc[fieldName] || [] }),
            abiJSON
        )
    abiDefinitions.serialize(buffer, abiJSON)
    serializedAbiHexString = Buffer.from(buffer.asUint8Array()).toString('hex')
    
    
    // create the setcode action
    const setCodeAction = [{
        account: 'eosio',
        name: 'setcode',
        authorization: [{
            actor: 'eosio',
            permission: 'active'
        }],
        data: {
            account: contractAccountName,
            vmtype: 0,
            vmversion: 0,
            code: wasm.toString('hex')
        }
    }];

    // create the setabi action
    const setAbiAction = [{
        account: 'eosio',
        name: 'setabi',
        authorization: [{
            actor: 'eosio',
            permission: 'active'
        }],
        data: {
            account: contractAccountName,
            abi: serializedAbiHexString
        }
    }];

    const serializedAbiAction = await api.serializeActions(setAbiAction);
    const serializedCodeAction = await api.serializeActions(setCodeAction);
    const abiData = serializedAbiAction[0].data;
    const codeData = serializedCodeAction[0].data;
    
    const proposalActionData = {
        proposer,
        proposal_name: proposalName,
        requested: signers,
        trx: {
              expiration: expirationDate,
              ref_block_num: 0,
              ref_block_prefix: 0,
              max_net_usage_words: 0,
              max_cpu_usage_ms: 0,
              delay_sec: 0,
              context_free_actions: [],
              actions: [
                {
                  account: 'eosio',
                  name: 'setcode',
                  authorization: [
                    {
                      actor: 'eosio',
                      permission: 'active'
                    }
                  ],
                  data: codeData
                },
                {
                  account: 'eosio',
                  name: 'setabi',
                  authorization: [
                    {
                      actor: 'eosio',
                      permission: 'active'
                    }
                  ],
                  data: abiData.toLowerCase()
                }
              ],
              transaction_extensions: []
            }
      };

      const transaction = {
        actions: [{
            account: 'eosio.msig',
            name: 'propose',
            authorization: [{
              actor: proposer,
              permission: 'active',
            }],
            data: proposalActionData,
          }]
      }

      fs.writeFileSync(`transaction.json`, JSON.stringify(transaction));
}

run()