
# Multisig tools for CLI 

If you are looking for a web-based tool, use https://msig.app/libre created by https://cryptolions.io

Create multisigs to update wasm and abi for any smart contract on Libre.

Requires you to have built the wasm and abi files.

Just specify the path and other variables in the js file:

```
const proposer = `yourAccount`;
const proposalName = 'proposalName' // makeid(10) - for random name
const contractAccountName = 'accountToUpdate'; // account executing transaction
const wasmPath = 'fullPathToWASM'; // replace with the path to your wasm file
const abiPath = 'fullPathToABI'; // replace with the path to your abi file
const requestedActor = 'eosio.prods';
const requestedActorPermission = 'active';
```
